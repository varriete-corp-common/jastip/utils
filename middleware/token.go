package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/varriete-corp-common/jastip/utils/nats/redis_management/command"
	"gitlab.com/varriete-corp-common/jastip/utils/nats/redis_management/dto"

	"gitlab.com/varriete-corp-common/jastip/utils/common"

	"github.com/gin-gonic/gin"
)

type MiddlewareToken struct{}

func (group MiddlewareToken) GetToken(c *gin.Context) (string, bool) {
	tokenString := c.GetHeader("Authorization")
	if tokenString == "" {
		return "", false
	}
	tokenString = tokenString[len("Bearer "):]

	if len(tokenString) > 2 {
		return tokenString, true
	}
	return "", false
}

func (group MiddlewareToken) CreateToken(user_id uint64) (common.TokenEntity, string, error) {
	id := strconv.Itoa(int(user_id))
	Bcrypt, _ := common.Encrypt{}.Bcrypt(id)
	idempotency_key_token := common.Encrypt{}.SHA256(Bcrypt)
	idempotency_key := common.Encrypt{}.SHA256(idempotency_key_token)

	jwt_data, _ := MiddlewareJwt{}.Create(uint64(user_id), 0)
	token := fmt.Sprintf("%v#^%v&%v", Bcrypt, jwt_data, idempotency_key_token)

	refresh_token, _ := MiddlewareJwt{}.Create(uint64(user_id), 1)

	data_store_cache := common.TokenEntity{
		Id:                   user_id,
		RefreshToken:         refresh_token,
		EncrypteToken:        Bcrypt,
		XIdempotencyKeyToken: idempotency_key_token,
		XIdempotencyKey:      idempotency_key,
	}

	dataval, _ := json.Marshal(data_store_cache)

	// -------------------------------- store cache

	var ch = make(chan func() common.StoreResponseHttp)
	key_cache := fmt.Sprintf("auth:%v", user_id)
	data_store := dto.DtosStore{
		Group:   "user",
		Key:     key_cache,
		Value:   string(dataval),
		Exp:     60,
		TypeExp: "minute",
	}
	go command.Create{}.Create(data_store, ch)
	defer close(ch)

	resp := (<-ch)()

	switch {
	case resp.Code == http.StatusCreated:
		return data_store_cache, token, nil
	default:
		return common.TokenEntity{}, "", errors.New(resp.ErrorResponse)
	}
}
