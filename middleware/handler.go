package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/varriete-corp-common/jastip/utils/config"
	model "gitlab.com/varriete-corp-common/jastip/utils/model/redis_management"
	model_user "gitlab.com/varriete-corp-common/jastip/utils/model/user_management"
	"gitlab.com/varriete-corp-common/jastip/utils/nats/redis_management/dto"
	"gitlab.com/varriete-corp-common/jastip/utils/nats/redis_management/query"

	"gitlab.com/varriete-corp-common/jastip/utils/nats/redis_management/command"

	"gitlab.com/varriete-corp-common/jastip/utils/common"
	"gitlab.com/varriete-corp-common/jastip/utils/handler"

	"github.com/gin-gonic/gin"
)

type MiddlewareHandler struct{}

func (Group MiddlewareHandler) Token(c *gin.Context) {
	cv := config.Config{}.ConfigVariable()

	tokenString := c.GetHeader("Authorization")
	idempoten_user := c.GetHeader("X-Idempotency-Key")
	platform := c.GetHeader("Platform")
	platform_version := c.GetHeader("PlatformVersion")
	MenuKey := c.GetHeader("MenuKey")
	MenuAction := c.GetHeader("MenuAction")

	switch {
	case len(tokenString) == 0 || len(idempoten_user) == 0 || len(platform) == 0 || len(platform_version) == 0:
		handler.Http{}.Response(c,
			common.StoreResponseHttp{
				ServiceName:     cv.Server.ServiceName,
				UserId:          0,
				Code:            http.StatusUnauthorized,
				Request:         tokenString,
				Response:        "",
				Data:            nil,
				MessageActivity: "handler access token and header",
				Message:         "unauthorized",
				Page:            common.DtoPageSuccess{},
				LogCode:         handler.Log{}.Code(),
				ErrorResponse:   "header rejected, please cek your documentation",
				Logging:         true,
			})
		return
	}

	token, cek := MiddlewareToken{}.GetToken(c)
	if cek {
		var ch = make(chan func() (uint64, error))
		go Group.TokenVerify(token, idempoten_user, ch)
		defer close(ch)
		user_id, err := (<-ch)()

		switch err {
		case nil:
			// cek_idempotent
			c.Set("user_id", user_id)
			c.Set("menu_key", MenuKey)
			c.Set("menu_action", MenuAction)
			// c.Set("token", token)
			c.Writer.Header().Set("Authorization", "Bearer "+token)
			c.Next()
		default:
			handler.Http{}.Response(c,
				common.StoreResponseHttp{
					ServiceName:     cv.Server.ServiceName,
					UserId:          0,
					Code:            http.StatusUnauthorized,
					Request:         token,
					Response:        "",
					Data:            nil,
					MessageActivity: "handler access token",
					Message:         "unauthorized",
					Page:            common.DtoPageSuccess{},
					LogCode:         handler.Log{}.Code(),
					ErrorResponse:   err.Error(),
					Logging:         true,
				})
			return
		}
	} else {
		handler.Http{}.Response(c,
			common.StoreResponseHttp{
				ServiceName:     cv.Server.ServiceName,
				UserId:          0,
				Code:            http.StatusUnauthorized,
				Request:         "",
				Response:        "",
				Data:            nil,
				MessageActivity: "handler access token",
				Message:         "unauthorized",
				Page:            common.DtoPageSuccess{},
				LogCode:         handler.Log{}.Code(),
				ErrorResponse:   errors.New("unauthorized").Error(),
				Logging:         true,
			})
		return
	}
}

func (Group MiddlewareHandler) TokenVerify(token string, idempoten_key_user string, resp_return chan func() (uint64, error)) {
	var user_id uint64
	var error_resp error

	cek_one := strings.Contains(token, "#^")
	cek_two := strings.Contains(token, "&")

	if cek_one && cek_two {
		explode := strings.Split(token, "#^")
		explode_2 := strings.Split(explode[1], "&")
		encrypte_token := explode[0]
		jwt_data := explode_2[0]
		idempoten_token := explode_2[1]
		// ------------------------------------------------------------------------ cek jwt verify
		var ch = make(chan func() (common.JwtEntity, error))
		go MiddlewareJwt{}.Verify(jwt_data, ch)
		defer close(ch)
		jwt_entity, err_verify := (<-ch)()

		switch err_verify {
		case nil:
			// ------------------------------------------------------------------------ cek bcrypte user_id
			jwt_id := strconv.Itoa(int(jwt_entity.Id))
			match_token_and_id := common.Encrypt{}.BcryptCheck(jwt_id, encrypte_token)

			switch {
			case match_token_and_id:
				// ----------------------------------------------- cek redis
				var ch = make(chan func() common.StoreResponseHttp)
				key_cache := "user:auth:" + jwt_id
				data_store := dto.DtosKey{
					Key: key_cache,
				}
				go query.Read{}.Read(data_store, ch)
				defer close(ch)

				resp := (<-ch)()

				// ----------------------------------------------- logic
				switch {
				case resp.Code == http.StatusOK:

					// ----------------------------------------------- decode claims jwt
					var data model.Redis
					jsonBytes, _ := json.Marshal(resp.Data)
					json.Unmarshal([]byte(jsonBytes), &data)

					var data_token common.TokenEntity
					json.Unmarshal([]byte(data.Value), &data_token)

					// -----------------------------------------------
					switch {
					case idempoten_token != data_token.XIdempotencyKeyToken: // cek idempoten token, jika idempotent yang di ada di bearer user berbeda dengan idempotent dari cache
						user_id = data_token.Id
						error_resp = errors.New("X-Idempotency-Key from token not verified")
					case idempoten_key_user != data_token.XIdempotencyKey: // cek idempoten user, jika idempotent yang di bawa user berbeda dengan idempotent dari cache
						user_id = data_token.Id
						error_resp = errors.New("X-Idempotency-Key from user not verified")
					default:

						// ----------------------------------------------- cek token expired
						var ch_exp = make(chan func() (common.JwtEntity, error))
						go MiddlewareJwt{}.Verify(data_token.RefreshToken, ch_exp)
						defer close(ch_exp)
						_, err_verify_exp := (<-ch_exp)()

						// -----------------------------------------------
						switch err_verify_exp {
						case nil:

							// -------------------------------- store cache
							refresh_token, _ := MiddlewareJwt{}.Create(uint64(data_token.Id), 1)
							data_token.RefreshToken = refresh_token

							dataval, _ := json.Marshal(data_token)

							var ch_cache = make(chan func() common.StoreResponseHttp)
							key_cache := fmt.Sprintf("auth:%v", data_token.Id)
							data_store_cache := dto.DtosStore{
								Group:   "user",
								Key:     key_cache,
								Value:   string(dataval),
								Exp:     60,
								TypeExp: "minute",
							}
							go command.Create{}.Create(data_store_cache, ch_cache)
							defer close(ch_cache)

							resp_cache := (<-ch_cache)()
							switch resp_cache.Code {
							case 201:
								user_id = data_token.Id
								error_resp = nil
							default:
								user_id = 0
								error_resp = errors.New(resp_cache.ErrorResponse)
							}
						default:
							user_id = data_token.Id
							error_resp = err_verify_exp
						}
					}
				default:
					user_id = 0
					error_resp = errors.New("you are logged out")
				}
			default:
				user_id = 0
				error_resp = errors.New("access token not match")
			}
		default:
			user_id = 0
			error_resp = err_verify
		}
	} else {
		user_id = 0
		error_resp = errors.New("access token not match")
	}
	// ------------------------------------------------------------------------

	resp_return <- (func() (uint64, error) { return user_id, error_resp })
}

func (Group MiddlewareHandler) Access(MenuKey int, MenuAction string, User model_user.User) (common.StoreResponseHttp, error) {
	Cfg := config.Config{}.ConfigVariable()

	var AccessInfo bool
	for _, v := range User.UserRoleMap {
		for _, vv := range v.UserRole.UserRoleMenu {
			switch {
			case vv.UserMenu.Key == MenuKey:

				switch MenuAction {
				case "view":
					AccessInfo = vv.AccessView
				case "create":
					AccessInfo = vv.AccessCreate
				case "update":
					AccessInfo = vv.AccessUpdate
				case "delete":
					AccessInfo = vv.AccessDelete
				case "import":
					AccessInfo = vv.AccessImport
				case "export":
					AccessInfo = vv.AccessExport
				case "approval":
					AccessInfo = vv.AccessApproval
				}

			}
		}
	}

	switch AccessInfo {
	case false:
		ReqJson := common.Map{
			"menu_key":    MenuKey,
			"menu_action": MenuAction,
		}
		Requse, _ := json.Marshal(ReqJson)

		return common.StoreResponseHttp{
			ServiceName:     Cfg.Server.ServiceName,
			UserId:          User.Id,
			Code:            http.StatusMethodNotAllowed,
			Request:         string(Requse),
			Response:        "",
			Data:            false,
			MessageActivity: "access denied",
			Message:         "access denied",
			Page:            common.DtoPageSuccess{},
			LogCode:         handler.Log{}.Code(),
			ErrorResponse:   "access denied",
			Logging:         true,
		}, errors.New("access denied")
	default:
		return common.StoreResponseHttp{}, nil
	}
}
