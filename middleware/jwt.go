package middleware

import (
	"errors"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/varriete-corp-common/jastip/utils/common"
)

type MiddlewareJwt struct{}

var JwtSecretKey = []byte("p9eG7R9IoaFCHGB3HcikAaCyR5QSraCGXnwcMcA1vGHkSvfrDcMDLhMXwxpZ")

func (group MiddlewareJwt) Create(id uint64, exp_hour int) (string, error) { // https://github.com/golang-jwt/jwt/blob/main/example_test.go

	var registered_claims jwt.RegisteredClaims
	switch exp_hour {
	case 0:
		registered_claims = jwt.RegisteredClaims{}
	default:
		expired := common.Time{}.AddHours(exp_hour)
		registered_claims = jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expired),
			IssuedAt:  jwt.NewNumericDate(common.Time{}.TimeNow()),
			NotBefore: jwt.NewNumericDate(common.Time{}.TimeNow()),
		}
	}

	claims := common.JwtEntity{
		Id:               id,
		RegisteredClaims: registered_claims}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(JwtSecretKey)
	switch err {
	case nil:
		return tokenString, nil
	default:
		return "", err
	}
}

func (group MiddlewareJwt) Verify(req string, resp_return chan func() (common.JwtEntity, error)) {
	var response common.JwtEntity
	var resp_error error

	claims := common.JwtEntity{}

	tkn, err := jwt.ParseWithClaims(
		req, &claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(JwtSecretKey), nil
		})

	switch err {
	case nil:
		if tkn.Valid {
			response = claims
		} else {
			resp_error = errors.New("invalid token")
		}
	default:
		resp_error = err
	}

	resp_return <- (func() (common.JwtEntity, error) { return response, resp_error })
}
