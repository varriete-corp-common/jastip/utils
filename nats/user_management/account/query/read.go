package query

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/varriete-corp-common/jastip/utils/common"
	"gitlab.com/varriete-corp-common/jastip/utils/config"
	"gitlab.com/varriete-corp-common/jastip/utils/config/client"
)

type Read struct{}

func (group Read) Read(req common.ShowAccount, resp_return chan func() common.StoreResponseHttp) {
	nc := client.ClientNats
	cv := config.Config{}.ConfigVariable()

	// Subscribe to a unique reply subject, Use a unique subject
	replySubject := "reply.user_management.user.account.read_one." + cv.Server.Environment

	// -------------------------------------- execute
	sub, err_subscribe := nc.SubscribeSync(replySubject)
	if err_subscribe != nil {
		fmt.Println("err_subscribe : ", err_subscribe)
		log.Fatal(err_subscribe)
	}
	defer sub.Unsubscribe()

	// -------------------------------------- manipulate data
	request, _ := json.Marshal(req)

	// -------------------------------------- publish
	err_publish := nc.PublishRequest("user_management.user.account.read_one."+cv.Server.Environment, replySubject, []byte(request))
	if err_publish != nil {
		fmt.Println("err_publish : ", err_publish)
		log.Fatal(err_publish)
	}

	// -------------------------------------- get response after publish
	msg, err_next_message := sub.NextMsg(time.Second * 10) // Adjust timeout as needed
	if err_next_message != nil {
		fmt.Println("err_next_message : ", err_next_message)
		log.Fatal(err_next_message)
	}

	response := string(msg.Data)

	// -------------------------------------- decode response
	data := common.StoreResponseHttp{}
	json.Unmarshal([]byte(response), &data)

	// -------------------------------------- return
	resp_return <- (func() common.StoreResponseHttp { return data })
}
