package dto

type DtosStore struct {
	Group   string `json:"group" validate:"required"`
	Key     string `json:"key" validate:"required"`
	Value   string `json:"value" validate:"required"`
	Exp     int    `json:"exp"`
	TypeExp string `json:"type_exp" validate:"required,oneof=hour minute second no"` // hour, minute, second, no
}

type DtosUpdate struct {
	Key     string `json:"key" validate:"required"`
	Value   string `json:"value" validate:"required"`
	Exp     int    `json:"exp"`
	TypeExp string `json:"type_exp" validate:"required,oneof=hour minute second no"` // hour, minute, second, no
}

type DtosKey struct {
	Key string `json:"key" validate:"required"`
}
