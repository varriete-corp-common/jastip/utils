package config

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/viper"
)

type Config struct{}

type Variable struct {
	Server struct {
		ServiceName  string
		Port         string
		IngresPrefix string
		Environment  string
	}
	Cache struct {
		Redis struct {
			Host     string
			Port     string
			Password string
			Db       int
		}
	}
	NoSql struct {
		MongoDb struct {
			Url  string
			Name string
		}
	}
	Sql struct {
		Postgre struct {
			Master struct {
				Host     string
				Username string
				Password string
				Name     string
				Port     string
			}
			Replica struct {
				Host     string
				Username string
				Password string
				Name     string
				Port     string
			}
		}
	}
	MicroservicesProvider struct {
		Nats struct {
			Url           string
			Retry         bool
			MaxReconnects int
			ReconnectWait time.Duration
		}
	}
	TimeLocation string
}

func (Group Config) ConfigVariable() Variable {
	var Cfg Variable
	// -------------------------------------- server
	Cfg.Server.ServiceName = viper.GetString("service_name")
	Cfg.Server.Port = viper.GetString("server.port")
	Cfg.Server.IngresPrefix = viper.GetString("server.ingres_prefix")
	Cfg.Server.Environment = viper.GetString("server.environtment")

	// -------------------------------------- cache - redis
	Cfg.Cache.Redis.Host = viper.GetString("cache.redis.host")
	Cfg.Cache.Redis.Port = viper.GetString("cache.redis.port")
	Cfg.Cache.Redis.Password = viper.GetString("cache.redis.password")
	Cfg.Cache.Redis.Db = viper.GetInt("cache.redis.db")

	// -------------------------------------- nosql - mongodb
	Cfg.NoSql.MongoDb.Url = viper.GetString("database.mongo.url")
	Cfg.NoSql.MongoDb.Name = viper.GetString("database.mongo.name")

	// -------------------------------------- sql - postgre
	// -------------------- master
	Cfg.Sql.Postgre.Master.Host = viper.GetString("database.postgre.host")
	Cfg.Sql.Postgre.Master.Username = viper.GetString("database.postgre.username")
	Cfg.Sql.Postgre.Master.Password = viper.GetString("database.postgre.password")
	Cfg.Sql.Postgre.Master.Name = viper.GetString("database.postgre.name")
	Cfg.Sql.Postgre.Master.Port = viper.GetString("database.postgre.port")
	// -------------------- replica
	Cfg.Sql.Postgre.Replica.Host = viper.GetString("database.postgre_replica.host")
	Cfg.Sql.Postgre.Replica.Username = viper.GetString("database.postgre_replica.username")
	Cfg.Sql.Postgre.Replica.Password = viper.GetString("database.postgre_replica.password")
	Cfg.Sql.Postgre.Replica.Name = viper.GetString("database.postgre_replica.name")
	Cfg.Sql.Postgre.Replica.Port = viper.GetString("database.postgre_replica.port")

	// -------------------------------------- microservices provider - NATS
	Cfg.MicroservicesProvider.Nats.Url = viper.GetString("nats.server.nats_1")
	Cfg.MicroservicesProvider.Nats.Retry = viper.GetBool("nats.retry")
	Cfg.MicroservicesProvider.Nats.MaxReconnects = viper.GetInt("nats.max_reconnects")
	ReconnectWait := time.Duration(viper.GetInt("nats.reconnect_wait_second") * int(time.Second))
	Cfg.MicroservicesProvider.Nats.ReconnectWait = ReconnectWait

	// -------------------------------------- time location
	Cfg.TimeLocation = viper.GetString("time_format.time_location")

	return Cfg
}

func (Group Config) ConfigPath() {

	viper.AddConfigPath("./environment")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if Err := viper.ReadInConfig(); Err != nil {
		// fmt.Println("Error reading config file : ", err.Error())
		fmt.Println("Error reading config file : ", Err.Error())
		os.Exit(0)
	}

	// viper.MergeInConfig()

	// viper.AddConfigPath(CONFIG_PATH)
	// viper.SetConfigName(ServiceConfigName)
	// viper.SetConfigType(ServiceConfigType)
	// viper.MergeInConfig()
}
