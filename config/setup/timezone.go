package setup

// import (
// 	"encoding/json"
// 	"fmt"
// 	"net/http"

// 	"github.com/spf13/viper"
// )

// type Timezone struct{}

// var Status string
// var Country string
// var CountryCode string
// var Region string
// var RegionName string
// var City string
// var Zip string
// var Latitude float64
// var Longitude float64
// var TimezoneLoc string
// var Isp string
// var Org string
// var As string
// var Query string

// type TimezoneLocation struct {
// 	Status      string  `json:"status"`
// 	Country     string  `json:"country"`
// 	CountryCode string  `json:"countryCode"`
// 	Region      string  `json:"region"`
// 	RegionName  string  `json:"regionName"`
// 	City        string  `json:"city"`
// 	Zip         string  `json:"zip"`
// 	Latitude    float64 `json:"lat"`
// 	Longitude   float64 `json:"lon"`
// 	TimezoneLoc string  `json:"timezone"`
// 	Isp         string  `json:"isp"`
// 	Org         string  `json:"org"`
// 	As          string  `json:"as"`
// 	Query       string  `json:"query"`
// }

// func (cstz Timezone) Open() {
// 	response, err := http.Get("http://ip-api.com/json/")
// 	if err != nil {
// 		fmt.Println("timezone err : ", err)
// 		returndata(TimezoneLocation{}, "viper")
// 	} else {
// 		var location TimezoneLocation
// 		if err2 := json.NewDecoder(response.Body).Decode(&location); err2 != nil {
// 			fmt.Println("timezone err : ", err2)
// 			returndata(TimezoneLocation{}, "viper")
// 		}

// 		returndata(location, "")
// 		defer response.Body.Close()
// 	}
// }

// func returndata(location TimezoneLocation, cekviper string) {
// 	if cekviper == "viper" {
// 		Status = "success"
// 		Country = "Indonesia"
// 		CountryCode = ""
// 		Region = ""
// 		RegionName = ""
// 		City = ""
// 		Zip = ""
// 		Latitude = 0
// 		Longitude = 0
// 		TimezoneLoc = viper.GetString("time_format.time_location")
// 		Isp = ""
// 		Org = ""
// 		As = ""
// 		Query = ""
// 	} else {
// 		Status = location.Status
// 		Country = location.Country
// 		CountryCode = location.CountryCode
// 		Region = location.Region
// 		RegionName = location.RegionName
// 		City = location.City
// 		Zip = location.Zip
// 		Latitude = location.Latitude
// 		Longitude = location.Longitude
// 		TimezoneLoc = location.TimezoneLoc
// 		Isp = location.Isp
// 		Org = location.Org
// 		As = location.As
// 		Query = location.Query
// 	}
// }

// func (cctz Timezone) Read() TimezoneLocation {
// 	return TimezoneLocation{
// 		Status:      Status,
// 		Country:     Country,
// 		CountryCode: CountryCode,
// 		Region:      Region,
// 		RegionName:  RegionName,
// 		City:        City,
// 		Zip:         Zip,
// 		Latitude:    Latitude,
// 		Longitude:   Longitude,
// 		TimezoneLoc: TimezoneLoc,
// 		Isp:         Isp,
// 		Org:         Org,
// 		As:          As,
// 		Query:       Query,
// 	}
// }
