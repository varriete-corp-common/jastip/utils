package setup

import (
	"fmt"

	"gitlab.com/varriete-corp-common/jastip/utils/config"
	"gitlab.com/varriete-corp-common/jastip/utils/config/client"
)

type Setup struct{}

func (Group Setup) Open(OpenConnection []string) {
	// ----------------------------- put variable config
	config.Config{}.ConfigPath()
	Cfg := config.Config{}.ConfigVariable()

	// ----------------------------- connect client
	for _, v := range OpenConnection {
		switch {
		case v == "cache":
			client.Redis{}.Open(Cfg)
		case v == "nosql":
			client.MongoDb{}.Open(Cfg)
		case v == "provider":
			client.Nats{}.Open(Cfg)
		case v == "sql":
			client.Postgre{}.Open(Cfg)
		default:
			fmt.Println("-------------------- No Connection Available --------------------")
		}
	}
}

func (Group Setup) Close(OpenConnection []string) {
	for _, v := range OpenConnection {
		switch {
		case v == "cache":
			client.Redis{}.Close()
		case v == "nosql":
			client.MongoDb{}.Close()
		case v == "provider":
			client.Nats{}.Close()
		case v == "sql":
			client.Postgre{}.Close()
		default:
			fmt.Println("-------------------- No Close Connection Available --------------------")
		}
	}
}
