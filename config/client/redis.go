package client

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/varriete-corp-common/jastip/utils/config"

	"github.com/redis/go-redis/v9"
)

type Redis struct{}

var ClientRedis *redis.Client

func (Group Redis) Connect(Cfg config.Variable) error {

	Host := Cfg.Cache.Redis.Host
	Port := Cfg.Cache.Redis.Port
	Password := Cfg.Cache.Redis.Password
	Db := Cfg.Cache.Redis.Db

	Address := Host + ":" + Port

	Ctx := context.TODO()

	Client := redis.NewClient(&redis.Options{
		Addr:            Address,
		Password:        Password,
		DB:              Db,
		PoolSize:        10000,
		MinIdleConns:    1,
		MaxIdleConns:    10000,
		ConnMaxIdleTime: 10 * time.Minute,
		ConnMaxLifetime: 10 * time.Minute,
		PoolTimeout:     10 * time.Minute,
		ReadTimeout:     10 * time.Minute,
		WriteTimeout:    10 * time.Minute,
	})

	TimeExp := 10 * time.Second
	Err := Client.Set(Ctx, "ping", "pong", TimeExp).Err()
	if Err != nil {
		return Err
	}

	ClientRedis = Client

	return nil
}

func (Group Redis) Open(Cfg config.Variable) {
	Err := Redis{}.Connect(Cfg)
	if Err != nil {
		fmt.Println("-------------------- Redis Connection Error : ", Err.Error())
		os.Exit(0)
	}
	fmt.Println("-------------------- Redis Connection Success --------------------")
}

func (Group Redis) Close() {
	Err := ClientRedis.Close()
	if Err != nil {
		fmt.Println("-------------------- Redis Close Connection Error : ", Err.Error())
	}
	fmt.Println("-------------------- Redis Close Connection Success --------------------")
}
