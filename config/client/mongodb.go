package client

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/varriete-corp-common/jastip/utils/config"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDb struct{}

var ClientMongoDb *mongo.Client

func (Group MongoDb) Connect(Cfg config.Variable) error {

	// loggerOptions := options.
	// 	Logger().
	// 	SetComponentLevel(options.LogComponentCommand, options.LogLevelDebug)

	// Set client options
	ClientOptions := options.Client().
		ApplyURI(Cfg.NoSql.MongoDb.Url)
		// SetLoggerOptions(loggerOptions)

	// Connect to MongoDB
	Client, Err := mongo.Connect(context.TODO(), ClientOptions)

	// Return error if there problem
	if Err != nil {
		return Err
	}

	ClientMongoDb = Client

	return nil
}

func (Group MongoDb) Open(Cfg config.Variable) {
	Err := MongoDb{}.Connect(Cfg)
	if Err != nil {
		fmt.Println("-------------------- MongoDb Connection Error : ", Err.Error())
		os.Exit(0)
	}
	fmt.Println("-------------------- MongoDb Connection Success --------------------")
}

func (Group MongoDb) Close() {
	Err := ClientMongoDb.Disconnect(context.TODO())
	if Err != nil {
		fmt.Println("-------------------- MongoDb Close Connection Error : ", Err.Error())
	}
	fmt.Println("-------------------- MongoDb Close Connection Success --------------------")
}
