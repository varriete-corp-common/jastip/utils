package client

import (
	"fmt"
	"os"

	"gitlab.com/varriete-corp-common/jastip/utils/config"

	"github.com/nats-io/nats.go"
)

type Nats struct{}

var ClientNats *nats.Conn

func (Group Nats) Connect(Cfg config.Variable) error {
	// -------------------------------------- connect
	Nc, Err := nats.Connect(Cfg.MicroservicesProvider.Nats.Url, //nats.DefaultURL,
		nats.RetryOnFailedConnect(Cfg.MicroservicesProvider.Nats.Retry),
		nats.MaxReconnects(Cfg.MicroservicesProvider.Nats.MaxReconnects), //10
		nats.ReconnectWait(Cfg.MicroservicesProvider.Nats.ReconnectWait),
		nats.ReconnectHandler(func(_ *nats.Conn) {
			// Note that this will be invoked for the first asynchronous connect.
			fmt.Println("-------------------- NATS Re-Connecting --------------------")
			Group.Connect(Cfg)
		}))
	if Err != nil {
		// Should not return an error even if it can't connect, but you still
		// need to check in case there are some configuration errors.
		return Err
	}

	ClientNats = Nc

	return nil
}

func (Group Nats) Open(Cfg config.Variable) {
	Err := Nats{}.Connect(Cfg)
	if Err != nil {
		fmt.Println("-------------------- NATS Connection Error : ", Err.Error())
		os.Exit(0)
	}
	fmt.Println("-------------------- NATS Connection Success --------------------")
}

func (Group Nats) Close() {
	ClientNats.Close()
	fmt.Println("-------------------- NATS Close Connection Success --------------------")
}
