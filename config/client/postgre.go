package client

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/varriete-corp-common/jastip/utils/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"gorm.io/plugin/dbresolver"
)

type Postgre struct{}

var ClientPostgre *gorm.DB

const ConnPostgrePattern = "host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=%s"

func (Group Postgre) Connect(Cfg config.Variable) error {
	Conn := fmt.Sprintf(
		ConnPostgrePattern,
		Cfg.Sql.Postgre.Master.Host,
		Cfg.Sql.Postgre.Master.Username,
		Cfg.Sql.Postgre.Master.Password,
		Cfg.Sql.Postgre.Master.Name,
		Cfg.Sql.Postgre.Master.Port,
		Cfg.TimeLocation,
	)

	Client, Err := gorm.Open(postgres.Open(Conn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	if Err != nil {
		fmt.Println("-------------------- Postgre Error Connection : ", Err)
		return Err
	}

	ReplicaOne := fmt.Sprintf(
		ConnPostgrePattern,
		Cfg.Sql.Postgre.Replica.Host,
		Cfg.Sql.Postgre.Replica.Username,
		Cfg.Sql.Postgre.Replica.Password,
		Cfg.Sql.Postgre.Replica.Name,
		Cfg.Sql.Postgre.Replica.Port,
		Cfg.TimeLocation,
	)

	if ErrRe := Client.Use(dbresolver.Register(dbresolver.Config{
		Replicas: []gorm.Dialector{
			postgres.Open(ReplicaOne),
		},
		// sources/replicas load balancing policy
		Policy: dbresolver.RandomPolicy{},
		// print sources/replicas mode in logger
		TraceResolverMode: true,
	}).SetConnMaxIdleTime(time.Hour).
		SetConnMaxLifetime(time.Minute * 5).
		SetMaxIdleConns(10).
		SetMaxOpenConns(20)); ErrRe != nil {
		fmt.Println("-------------------- Postgre Error Connection Replica : ", ErrRe)
		return ErrRe
	}

	ClientPostgre = Client.Debug()

	return nil
}

func (Group Postgre) Open(Cfg config.Variable) {
	Err := Postgre{}.Connect(Cfg)
	if Err != nil {
		fmt.Println("-------------------- Postgre Connection Error : ", Err.Error())
		os.Exit(0)
	}
	fmt.Println("-------------------- Postgre Connection Success --------------------")
}

func (Group Postgre) Close() error {
	db, Err := ClientPostgre.DB()
	if Err != nil {
		fmt.Println("-------------------- Postgre Close Connection Error : ", Err.Error())
		return Err
	}
	db.Close()
	fmt.Println("-------------------- Postgre Close Connection Success --------------------")
	return nil
}
