package model

type Redis struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
