package model

type UserAddress struct {
	Id        uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	UserId    uint64 `json:"user_id" gorm:"not_null" bson:"user_id"`
	Address   string `json:"address" gorm:"not null;" bson:"address"`
	Long      string `json:"long" gorm:"default:null;" bson:"long"`
	Lat       string `json:"lat" gorm:"default:null;" bson:"lat"`
	Default   bool   `json:"default" gorm:"default:false;" bson:"default"`
	CreatedAt string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`

	// User User `json:"user,omitempty" gorm:"foreignKey:user_id;references:id" bson:"user,omitempty"`
}

func (group UserAddress) TableName() string {
	return "user_address"
}
