package model

type UserRoleMenu struct {
	Id             uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	UserRoleId     uint64 `json:"user_role_id" gorm:"not_null" bson:"user_role_id"`
	UserMenuId     uint64 `json:"user_menu_id" gorm:"not_null" bson:"user_menu_id"`
	AccessView     bool   `json:"access_view" gorm:"default:false;" bson:"access_view"`
	AccessCreate   bool   `json:"access_create" gorm:"default:false;" bson:"access_create"`
	AccessUpdate   bool   `json:"access_update" gorm:"default:false;" bson:"access_update"`
	AccessDelete   bool   `json:"access_delete" gorm:"default:false;" bson:"access_delete"`
	AccessImport   bool   `json:"access_import" gorm:"default:false;" bson:"access_import"`
	AccessExport   bool   `json:"access_export" gorm:"default:false;" bson:"access_export"`
	AccessApproval bool   `json:"access_approval" gorm:"default:false;" bson:"access_approval"`
	CreatedAt      string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt      string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt      string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy      uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy      uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy      uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`

	UserRole UserRole `json:"user_role,omitempty" gorm:"foreignKey:id;references:user_role_id" bson:"user_role,omitempty"`
	UserMenu UserMenu `json:"user_menu,omitempty" gorm:"foreignKey:id;references:user_menu_id" bson:"user_menu,omitempty"`
}

func (group UserRoleMenu) TableName() string {
	return "user_role_menu"
}
