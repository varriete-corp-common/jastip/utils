package model

// Type      string `json:"type" binding:"required,oneof=message attachment voice notif separator_name" bson:"type" gorm:"default:message"`

type User struct {
	Id       uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	Name     string `json:"name" gorm:"default:null;" bson:"name"`
	Password string `json:"password" gorm:"not null;" bson:"password"`
	Avatar   string `json:"avatar" gorm:"default:null;" bson:"avatar"`
	Gender   string `json:"gender" gorm:"default:male;type:enum('male','female')" bson:"gender"`
	// UserRoleMapId uint64 `json:"user_role_map_id" gorm:"not_null" bson:"user_role_map_id"`

	CreatedAt string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`

	UserRoleMap []UserRoleMap `json:"user_role_map,omitempty" bson:"user_role_map,omitempty"`
	UserEmail   []UserEmail   `json:"user_email,omitempty" gorm:"foreignKey:user_id;references:id" bson:"user_email,omitempty"`
	UserPhone   []UserPhone   `json:"user_phone,omitempty" gorm:"foreignKey:user_id;references:id" bson:"user_phone,omitempty"`
	UserAddress []UserAddress `json:"user_address,omitempty" gorm:"foreignKey:user_id;references:id" bson:"user_address,omitempty"`
}

func (group User) TableName() string {
	return "user"
}
