package model

type UserMenu struct {
	Id             uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	Name           string `json:"name" gorm:"default:null;" bson:"name"`
	Icon           string `json:"icon" gorm:"default:null;" bson:"icon"`
	Path           string `json:"path" gorm:"default:null;" bson:"path"`
	Separator      bool   `json:"separator" gorm:"default:false;" bson:"separator"`
	Header         bool   `json:"header" gorm:"default:false;" bson:"header"`
	Key            int    `json:"key" gorm:"default:1;" bson:"key"`
	KeyGroup       int    `json:"key_group" gorm:"default:1;" bson:"key_group"`
	Level          int    `json:"level" gorm:"default:1;" bson:"level"`
	Order          int    `json:"order" gorm:"default:1;" bson:"order"`
	OptionView     bool   `json:"option_view" gorm:"default:false;" bson:"option_view"`
	OptionCreate   bool   `json:"option_create" gorm:"default:false;" bson:"option_create"`
	OptionUpdate   bool   `json:"option_update" gorm:"default:false;" bson:"option_update"`
	OptionDelete   bool   `json:"option_delete" gorm:"default:false;" bson:"option_delete"`
	OptionImport   bool   `json:"option_import" gorm:"default:false;" bson:"option_import"`
	OptionExport   bool   `json:"option_export" gorm:"default:false;" bson:"option_export"`
	OptionApproval bool   `json:"option_approval" gorm:"default:false;" bson:"option_approval"`
	CreatedAt      string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt      string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt      string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy      uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy      uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy      uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`
}

func (group UserMenu) TableName() string {
	return "user_menu"
}
