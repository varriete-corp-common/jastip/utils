package model

type UserRole struct {
	Id        uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	Name      string `json:"name" gorm:"default:null;" bson:"name"`
	CreatedAt string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`

	UserRoleMenu []UserRoleMenu `json:"user_role_menu,omitempty" gorm:"foreignKey:user_role_id;references:id" bson:"user_role_menu,omitempty"`
}

func (group UserRole) TableName() string {
	return "user_role"
}
