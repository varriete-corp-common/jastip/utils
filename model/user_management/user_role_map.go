package model

type UserRoleMap struct {
	Id         uint64 `json:"id" gorm:"primaryKey" bson:"id"`
	UserId     uint64 `json:"user_id" gorm:"not_null" bson:"user_id"`
	UserRoleId uint64 `json:"user_role_id" gorm:"not_null" bson:"user_role_id"`
	CreatedAt  string `json:"created_at" gorm:"default:null;" bson:"created_at"`
	UpdatedAt  string `json:"updated_at" gorm:"default:null;" bson:"updated_at"`
	DeletedAt  string `json:"deleted_at" gorm:"default:null;" bson:"deleted_at"`
	CreatedBy  uint64 `json:"created_by" gorm:"default:null;" bson:"created_by"`
	UpdatedBy  uint64 `json:"updated_by" gorm:"default:null;" bson:"updated_by"`
	DeletedBy  uint64 `json:"deleted_by" gorm:"default:null;" bson:"deleted_by"`

	UserRole UserRole `json:"user_role,omitempty" gorm:"foreignKey:id;references:user_role_id" bson:"user_role,omitempty"`
}

func (group UserRoleMap) TableName() string {
	return "user_role_map"
}
