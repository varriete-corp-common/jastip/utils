BEGIN;

-------------------------------- user role 
create index IF NOT EXISTS idx_name on user_role (name);
-------------------------------- user
create index IF NOT EXISTS idx_name on user (name);
create index IF NOT EXISTS idx_gender on user (gender);
-------------------------------- user_email 
create index IF NOT EXISTS idx_email on user_email (email);
-------------------------------- user_phone
create index IF NOT EXISTS idx_phone on user_phone (phone);
-------------------------------- user_address
create index IF NOT EXISTS idx_address on user_address (address);

COMMIT;