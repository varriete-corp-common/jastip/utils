package handler

import (
	"context"
	"net/http"
	"strconv"

	"gitlab.com/varriete-corp-common/jastip/utils/common"
	"gitlab.com/varriete-corp-common/jastip/utils/config"
	"gitlab.com/varriete-corp-common/jastip/utils/config/client"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Log struct{}

func (Group Log) Code() string {
	IdLog := common.Encrypt{}.IdGenerate()
	LogCode := strconv.Itoa(int(IdLog))
	return LogCode
}

func (Group Log) Activity(
	bearer string,
	client_ip string,
	service_name string,
	log_code string,
	level string,
	user_id uint64,
	platform string,
	platform_version string,
	method string,
	url string,
	http_status_code int,
	http_status_message string,
	request string,
	response string,
	message string,
	message_activity string,
	error_response string,
	created_at string,
) {
	datastore := common.UserLogActivity{
		Bearer:          bearer,
		ServiceName:     service_name,
		ClientIp:        client_ip,
		LogCode:         log_code,
		Level:           level,
		UserId:          user_id,
		Platform:        platform,
		PlatformVersion: platform_version,
		Method:          method,
		URL:             url,
		HttpStatusCode:  http_status_code,
		HttpStatus:      http_status_message,
		Request:         request,
		Response:        response,
		Message:         message,
		MessageActivity: message_activity,
		ErrorResponse:   error_response,
		CreatedAt:       created_at,
	}

	Log{}.Create(datastore, "log_activity")
}

func (Group Log) Create(data any, collection string) (string, common.ResponseErrorRepository) { // ok
	cv := config.Config{}.ConfigVariable()

	// -------------- ex :
	dbconn := client.ClientMongoDb.Database(cv.NoSql.MongoDb.Name).Collection(collection)

	result, err := dbconn.InsertOne(context.TODO(), data)

	if err != nil {
		return "", common.ResponseErrorRepository{
			Error:          err,
			HttpStatusCode: http.StatusInternalServerError,
		}
	} else {
		dataresult := result.InsertedID
		return dataresult.(primitive.ObjectID).Hex(), common.ResponseErrorRepository{
			Error:          nil,
			HttpStatusCode: http.StatusOK,
		}
	}
}
