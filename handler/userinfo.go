package handler

import (
	"encoding/json"

	model "gitlab.com/varriete-corp-common/jastip/utils/model/user_management"
	"gitlab.com/varriete-corp-common/jastip/utils/nats/user_management/account/query"

	"gitlab.com/varriete-corp-common/jastip/utils/common"
)

type UserInfo struct{}

func (Group UserInfo) Read(Id uint64) (common.DtoResponseService, model.User) {
	var Ch = make(chan func() common.StoreResponseHttp)
	Filter := common.ShowAccount{
		Id: Id,
	}

	go query.Read{}.Read(Filter, Ch)
	defer close(Ch)

	Resp := (<-Ch)()

	switch Resp.Code {
	case 200:
		var data model.User
		jsonBytes, _ := json.Marshal(Resp.Data)
		json.Unmarshal([]byte(jsonBytes), &data)

		return common.DtoResponseService{
			Code:    Resp.Code,
			Message: Resp.Message,
			Error:   Resp.ErrorResponse,
			Page:    Resp.Page,
		}, data
	default:
		return common.DtoResponseService{
			Code:    Resp.Code,
			Message: Resp.Message,
			Error:   Resp.ErrorResponse,
		}, model.User{}
	}
}
