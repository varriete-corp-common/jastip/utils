package handler

import (
	"reflect"
	"strings"

	"gitlab.com/varriete-corp-common/jastip/utils/common"

	"github.com/go-playground/validator/v10"
)

type Validator struct{}

func (group Validator) Response(req validator.ValidationErrors, request any) []*common.IError {
	var errors []*common.IError
	for _, err := range req {
		var el common.IError
		fieldName := err.Field()
		field, _ := reflect.TypeOf(request).Elem().FieldByName(fieldName)
		fieldJSONName, _ := field.Tag.Lookup("json")
		el.Field = strings.ToLower((strings.Split(fieldJSONName, ","))[0])
		el.Tag = err.Tag()
		el.Value = err.Param()
		errors = append(errors, &el)
	}

	return errors
}
