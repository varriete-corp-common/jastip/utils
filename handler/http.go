package handler

import (
	"net/http"

	"gitlab.com/varriete-corp-common/jastip/utils/common"

	"github.com/gin-gonic/gin"
)

type Http struct{}

func (Group Http) Response(Ctx *gin.Context, Req common.StoreResponseHttp) {

	Bearer := Ctx.GetHeader("Authorization")

	var LevelLog string
	var StatusCodeMessage string

	switch Req.Code {
	case 200:
		Http{}.Success(Ctx, Req.Data, Req.Message, Req.Page, Req.LogCode)
		LevelLog = "success"
		StatusCodeMessage = "Ok"
	case 201:
		Http{}.Created(Ctx, Req.Data, Req.Message, Req.LogCode)
		LevelLog = "success"
		StatusCodeMessage = "Created"
	case 204:
		Http{}.NoContent(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "info"
		StatusCodeMessage = "No Content"
	case 304:
		Http{}.NotModified(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "info"
		StatusCodeMessage = "Not Modified"
	case 400:
		Http{}.BadRequest(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "warning"
		StatusCodeMessage = "Bad Request"
	case 401:
		Http{}.Unauthorized(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "info"
		StatusCodeMessage = "Unauthorized"
	case 404:
		Http{}.NotFound(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "info"
		StatusCodeMessage = "Not Found"
	case 405:
		Http{}.MethodNotAllowed(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "warning"
		StatusCodeMessage = "Method Not Allowed"
	case 409:
		Http{}.Conflic(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "warning"
		StatusCodeMessage = "Conflict"
	case 423:
		Http{}.Locked(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "warning"
		StatusCodeMessage = "Locked"
	case 500:
		Http{}.InternalServerError(Ctx, Req.Data, Req.Message, Req.ErrorResponse, Req.LogCode)
		LevelLog = "error"
		StatusCodeMessage = "Internal Server Error"
	}

	if Req.Logging {
		ClientIp := Ctx.ClientIP()

		Log{}.Activity(
			Bearer,
			ClientIp,
			Req.ServiceName,
			Req.LogCode,
			LevelLog,
			Req.UserId,
			Ctx.GetHeader("Platform"),
			Ctx.GetHeader("PlatformVersion"),
			Ctx.Request.Method,
			Ctx.Request.Host+Ctx.Request.URL.Path,
			Req.Code,
			StatusCodeMessage,
			Req.Request,
			Req.Response,
			Req.Message,
			Req.MessageActivity,
			Req.ErrorResponse,
			common.Time{}.TimeRfc3339())
	}

}

func (Group Http) Success(Ctx *gin.Context, Data any, Message string, Page common.DtoPageSuccess, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusOK, gin.H{
		"code":     http.StatusOK,
		"status":   "Ok",
		"data":     Data,
		"page":     Page,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) Created(Ctx *gin.Context, Data any, Message string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusCreated, gin.H{
		"code":     http.StatusCreated,
		"status":   "Created",
		"data":     Data,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) NoContent(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusNoContent, gin.H{
		"code":     http.StatusNoContent,
		"status":   "No Content",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) NotModified(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusNotModified, gin.H{
		"code":     http.StatusNotModified,
		"status":   "Not Modified",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) BadRequest(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
		"code":     http.StatusBadRequest,
		"status":   "Bad Request",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) Unauthorized(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
		"code":     http.StatusUnauthorized,
		"status":   "Unauthorized",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) NotFound(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
		"code":     http.StatusNotFound,
		"status":   "Not Found",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) MethodNotAllowed(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusMethodNotAllowed, gin.H{
		"code":     http.StatusMethodNotAllowed,
		"status":   "Method Not Allowed",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) InternalServerError(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
		"code":     http.StatusInternalServerError,
		"status":   "Internal Server Error",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) Conflic(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusConflict, gin.H{
		"code":     http.StatusConflict,
		"status":   "Conflict",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}

func (Group Http) Locked(Ctx *gin.Context, Data any, Message string, ErrorResponse string, LogCode string) {
	Ctx.AbortWithStatusJSON(http.StatusLocked, gin.H{
		"code":     http.StatusLocked,
		"status":   "Locked",
		"data":     Data,
		"error":    ErrorResponse,
		"message":  Message,
		"log_code": LogCode})
}
