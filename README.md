# Structure Folder
## GRPC
<pre>
├── asset/
│   ├── credential/
│   │   ├── gcloud.json
│   │   └── firebase.json
│   ├── file/
│   │   ├── noimage.jpg
│   │   └── excelfile.xls
├── common/
│   ├── asset/
│   ├── helper/
│   ├── library/
│   └── dto/
├── config/
│   ├── setup/
│   │   ├── postgre.go
│   │   ├── mongodb.go
│   │   ├── redis.go
│   │   └── elasticsearch.go
│   ├── entity/
│   │   ├── user.go
│   │   └── address.go
│   ├── migration/
│   │   ├── migrate.go
│   │   └── seeder.go
├── deployment/
│   ├── Dockerfile
│   └── Jenkinsfile
├── environment/
│   ├── config_prod.yaml
│   ├── config_dev.yaml
│   ├── config_stag.yaml
│   └── config.yaml
├── middleware/
│   └── jwt.go
├── src/
│   ├── application/
│   │   ├── user/
│   │   │   ├── controller/
│   │   │   │   └── controller.go
│   │   │   ├── dto/
│   │   │   │   └── dto.go
│   │   │   ├── grpc/
│   │   │   │	  ├── command/
│   │   │   │	  │   ├── create/
│   │   │   │	  │   │   └── create.proto
│   │   │   │	  │   ├── update/
│   │   │   │	  │   │   └── update.proto
│   │   │   │	  │   ├── delete/
│   │   │   │	  │   │   └── delete.proto
│   │   │   │	  ├── query/
│   │   │   │	  │   └── read.proto
│   │   │   │	  ├── client.go
│   │   │   │	  └── server.gogo mod edit -go 1.18
go mod tidy
│   │   │   ├── service/
│   │   │   │   └── service.go
│   │   │   ├── repository/
│   │   │   │   ├── command/
│   │   │   │   │   ├── postgre/
│   │   │   │   │   │   ├── create.go
│   │   │   │   │   │   ├── update.go
│   │   │   │   │   │   └── delete.go
│   │   │   │   │   ├── mongodb/
│   │   │   │   │   │   ├── create.go
│   │   │   │   │   │   ├── update.go
│   │   │   │   │   │   └── delete.go
│   │   │   │   ├── query/
│   │   │   │   │   ├── postgre.go
│   │   │   │   │   └── mongodb.go
│   │   │   │   └── repository.go
│   ├── route/
│   │   ├── user.go
│   │   └── address.go
│   ├── grpc_client/
│   │   ├── user/
│   │   │   ├── account/
│   │   │   │   ├── command/
│   │   │   │   │   ├── create/
│   │   │   │   │   │   └── create.proto
│   │   │   │   │   ├── update/
│   │   │   │   │   │   └── update.proto
│   │   │   │   │   ├── delete/
│   │   │   │   │   │   └── delete.proto
│   │   │   │   ├── query/
│   │   │   │   │   └── read.proto
│   │   │   │   └── client.go
│   └── main.go
├── README.md
├── go.mod
└── go.sum
</pre>

## NATS
<pre>
├── asset/
│   ├── credential/
│   │   ├── gcloud.json
│   │   └── firebase.json
│   ├── file/
│   │   ├── noimage.jpg
│   │   └── excelfile.xls
├── database/
│   ├── model/
│   │   ├── user.go
│   │   └── user_address.go
│   ├── script/
│   │   ├── create_table.sql
│   │   └── insert_table.sql
├── deployment/
│   ├── Dockerfile
│   └── Jenkinsfile
├── environment/
│   ├── config_prod.yaml
│   ├── config_dev.yaml
│   ├── config_stag.yaml
│   └── config.yaml
├── middleware/
│   └── jwt.go
├── src/
│   ├── application/
│   │   ├── user/
│   │   │   ├── controller/
│   │   │   │   └── controller.go
│   │   │   ├── dto/
│   │   │   │   └── dto.go
│   │   │   ├── service/
│   │   │   │   └── service.go
│   │   │   ├── repository/
│   │   │   │   ├── command/
│   │   │   │   │   ├── postgre/
│   │   │   │   │   │   ├── create.go
│   │   │   │   │   │   ├── update.go
│   │   │   │   │   │   └── delete.go
│   │   │   │   │   ├── mongodb/
│   │   │   │   │   │   ├── create.go
│   │   │   │   │   │   ├── update.go
│   │   │   │   │   │   └── delete.go
│   │   │   │   ├── query/
│   │   │   │   │   ├── postgre.go
│   │   │   │   │   └── mongodb.go
│   │   │   │   └── repository.go
│   ├── route/
│   │   ├── user.go
│   │   └── address.go
│   ├── nats/
│   │   ├── server/
│   │   │   ├── user/
│   │   │   │   ├── command/
│   │   │   │   │   ├── create.go
│   │   │   │   │   ├── delete.go
│   │   │   │   │   └── update.go
│   │   │   │   ├── query/
│   │   │   │   │   └── read.go
│   │   │   │   └── server.go
│   │   ├── client/
│   │   │   ├── redis/
│   │   │   │   ├── command/
│   │   │   │   │   ├── create.go
│   │   │   │   │   ├── delete.go
│   │   │   │   │   └── update.go
│   │   │   │   └── query/
│   │   │   │       └── read.go
│   └── main.go
├── utils/
│   ├── common/
│   │   ├── dto.go
│   │   ├── encrypt.go
│   │   ├── manipulate.go
│   │   ├── time.go
│   │   └── others.go
│   ├── config/
│   │   ├── client/
│   │   │   ├── mongodb.go
│   │   │   ├── redis.go
│   │   │   ├── nats.go
│   │   │   └── postgre.go
│   ├── handler/
│   │   ├── http.go
│   │   ├── log.go
│   │   └── validator.go
│   ├── middleware/
│   │   ├── jwt.go
│   │   └── others.go
├── README.md
├── go.mod
└── go.sum
</pre>
 
# Response Json
<pre>
status success
{
   "code" : 200,
   "status : "ok",
   "data" : {
   
   },
   "page" : {
   	"size"		: 10, // limit
   	"total"		: 100, // total data di database
   	"totalPages"	: 10, // total di bagi size
   	"current"	: 2, // user sedang buka page berapa
   },
   message:"",
   log_code:"20230911140200_2893"
 }
 
status error
 {
   "code" : 400,
   "status : "BAD_REQUEST",
   "data":{
   	"id"	: [
   		    "must not null",
   		    "only number",
   		    "required",
   		],
   	"name"	: [
   		    "required",
   		    "max length 200 character",
   		]
   },
   "error":"",
   message:"",
   log_code:"20230911140200_2893"
}
</pre>

# Install Package
<pre>
go get -u github.com/gin-gonic/gin
go get go.mongodb.org/mongo-driver/mongo
go get github.com/go-playground/validator/v10
go get -u gorm.io/gorm
go get gorm.io/plugin/soft_delete
go get -u gorm.io/driver/postgres
go get gorm.io/plugin/dbresolver
go get -u github.com/golang-jwt/jwt/v5
go get github.com/redis/go-redis/v9
go get github.com/spf13/viper
go get github.com/google/uuid
go get -u google.golang.org/grpc
go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

sudo apt install protobuf-compiler
sudo apt install golang-goprotobuf-dev

export GO_PATH=~/go
export PATH=$PATH:/$GO_PATH/bin
</pre>

# Golang Carbon 
https://github.com/golang-module/carbon
go get -u github.com/golang-module/carbon/v2

# Generate go.mod
go mod init myapps

# Url Example CRUD MongoDb
https://dev.to/hackmamba/build-a-rest-api-with-golang-and-mongodb-gin-gonic-version-269m

# Install Env
go get github.com/joho/godotenv

# Install Air Untuk Reload Serve Dev
<pre>
https://mainawycliffe.dev/blog/live-reloading-golang-using-air/

go install github.com/cosmtrek/air@latest

air -v
air init
buka file .air.toml > lalu arahkan main.go nya di line cmd
</pre>

# GRPC
<pre>
https://ahmadfadilah65.medium.com/implementasi-grpc-protobuf-rest-api-di-golang-b3d87a50999d
go get google.golang.org/grpc
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

-- create protoc go out grpc
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative src/application/grpc/command/create/create.proto

-- protoc for windows
https://github.com/protocolbuffers/protobuf/releases/tag/v21.12
https://www.geeksforgeeks.org/how-to-install-protocol-buffers-on-windows/

-- set environment
https://groups.google.com/g/golang-nuts/c/Qs8d56uavVs?pli=1
</pre>

# Kill Process In Port 
<pre>
-- kill
kill -9 $(lsof -t -i:3000 -sTCP:LISTEN)
-- show port
lsof -i tcp:8080
</pre>

# Go Get From Gitlab Private
<pre>
export GIT_TERMINAL_PROMPT=1
git config --global credential.helper store
git config --global credential.helper cache

$ git config --global url.git@gitlab.com:.insteadOf https://gitlab.com/
$ cat ~/.gitconfig
    [url "git@gitlab.com:"]
    > insteadOf = https://gitlab.com/
$ export GOPRIVATE=gitlab.com/varriete/master/common/global
$ go get gitlab.com/varriete/master/common/global
</pre>

# Update Module And All Import

## Ubuntu
<pre>
go mod edit -module {NEW_MODULE_NAME}
-- rename all imported module
find . -type f -name '*.go' \
  -exec sed -i -e 's,{OLD_MODULE},{NEW_MODULE},g' {} \;
</pre>

## OSX
<pre>
find . -type f -name '*.go' \
  -exec sed -i '' -e 's/{OLD_MODULE}/{NEW_MODULE}/g' {} \;
</pre>

## Windows
<pre>
# Replace these values with appropriate ones
$NEW_MODULE_NAME = "NEW"
$OLD_MODULE = "OLD"

go mod edit -module $NEW_MODULE_NAME

# Rename all imported modules in .go files
Get-ChildItem -Path . -Filter '*.go' -Recurse | ForEach-Object {
    $content = Get-Content -Path $_.FullName
    $updatedContent = $content -replace $OLD_MODULE, $NEW_MODULE_NAME
    $updatedContent | Set-Content -Path $filePath
}
</pre>

# NATS 
<pre>
example : https://github.com/nats-io/nats.go
golang implement : https://pkg.go.dev/github.com/nats-io/nats.go#section-readme


go get github.com/nats-io/nats.go/
go get github.com/nats-io/nats-server
</pre>

# Update Go Version Mod 
<pre>
go mod edit -go 1.21.6
go mod tidy
</pre>

https://dbdiagram.io/d/65c8af1dac844320aee459d4