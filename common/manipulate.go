package common

import "strconv"

type Manipulate struct{}

func (Group Manipulate) StringToInteger(Value string) uint64 {
	Resp, _ := strconv.Atoi(Value)
	return uint64(Resp)
}

func (Group Manipulate) IntegerToString(Value uint64) string {
	Resp := strconv.Itoa(int(Value))
	return Resp
}

func (Group Manipulate) StringToBool(Value string) bool {
	Resp, _ := strconv.ParseBool(Value)
	return Resp
}

func (Group Manipulate) StringToFloat(Value string, size int) float64 {
	Resp, _ := strconv.ParseFloat(Value, size)
	return Resp
}
