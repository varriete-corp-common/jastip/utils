package common

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"errors"

	"golang.org/x/crypto/bcrypt"
)

type Encrypt struct{}

func (Group Encrypt) Bcrypt(Password string) (string, error) {
	// bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	Bytes, Err := bcrypt.GenerateFromPassword([]byte(Password), bcrypt.DefaultCost)
	return string(Bytes), Err
}

func (Group Encrypt) BcryptCheck(Password, Hash string) bool {
	Err := bcrypt.CompareHashAndPassword([]byte(Hash), []byte(Password))
	return Err == nil
}

func (Group Encrypt) Base64Encoding(Data string) string {
	EncodedStr := base64.StdEncoding.EncodeToString([]byte(Data))
	return EncodedStr
}

func (Group Encrypt) Base64Decoding(Data string) (string, error) {
	DecodedStr, Err := base64.StdEncoding.DecodeString(Data)
	if Err != nil {
		return "", errors.New("malformed input")
	}
	return string(DecodedStr), nil
}

func (Group Encrypt) SHA256(Data string) string {
	H := sha256.New()
	H.Write([]byte(Data))
	Sha := hex.EncodeToString(H.Sum(nil))
	return Sha
}

func (Group Encrypt) SHA512(Data string) string {
	H := sha512.New()
	H.Write([]byte(Data))
	Sha := hex.EncodeToString(H.Sum(nil))
	return Sha
}

func (Group Encrypt) IdGenerate() int64 {
	return Time{}.TimeNowUnix("UnixMilli")
}
