package common

import (
	"github.com/golang-jwt/jwt/v5"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Map map[string]interface{} // sama seperti array object {}
type Mapany map[string]any      // sama seperti array object {}
type Mapstring map[string]string
type MapstUint map[string]uint64
type Interfaces []interface{} // sama seperti array [] kosong
type Interface interface{}    // sama seperti array [] kosong

type IError struct {
	Field string
	Tag   string
	Value string
}

type Datatable struct {
	Trash  int32  `json:"trash"`  // 0 or 1
	Limit  int32  `json:"limit"`  // 10 or other
	Page   int32  `json:"page"`   // 1
	Search string `json:"search"` // search fild
	// Order     string   `json:"order"`      // name_fild
	// Sort      string   `json:"sort"`       // asc or desc
	OrderSort []OrderSort `json:"order_sort"` // [name_1 asc, name_2 desc]
}

type OrderSort struct {
	Key  string `json:"key" bson:"key"`
	Sort string `json:"sort" bson:"sort"`
}

type KeyValueString struct {
	Key   string `json:"key" bson:"key"`
	Value string `json:"value" bson:"value"`
}

// type HttpClientResponse struct {
// 	Code int `json:"code"`
// 	Body any `json:"body"`
// }

type UserLogActivity struct {
	Id              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Bearer          string             `json:"bearer" bson:"bearer"`
	ClientIp        string             `json:"client_ip" bson:"client_ip"`
	ServiceName     string             `json:"service_name" bson:"service_name"`
	LogCode         string             `json:"log_code" bson:"log_code"`
	Level           string             `json:"level" bson:"level"`
	UserId          uint64             `json:"user_id" bson:"user_id"`
	Platform        string             `json:"platform" bson:"platform"`                 // web, android, ios or other
	PlatformVersion string             `json:"platform_version" bson:"platform_version"` // version web browser or mobile browser
	Method          string             `json:"method" bson:"method"`                     // POST,PUT,GET,DELETE :  c.Request.Method
	URL             string             `json:"url" bson:"url"`                           // c.Request.Host + c.Request.URL.Path
	HttpStatusCode  int                `json:"http_status_code" bson:"http_status_code"` // 200 - 500
	HttpStatus      string             `json:"http_status" bson:"http_status"`           // OK, BAD REQUEST, NOT FOUND or others
	Request         string             `json:"request" bson:"request"`
	Response        string             `json:"response" bson:"response"`
	Message         string             `json:"message" bson:"message"`
	MessageActivity string             `json:"message_activity" bson:"message_activity"`
	ErrorResponse   string             `json:"error_response" bson:"error_response"`
	CreatedAt       string             `json:"created_at" bson:"created_at"`
}

type ResponseErrorRepository struct {
	Error          error `json:"error" bson:"error"`
	HttpStatusCode int   `json:"http_status_code" bson:"http_status_code"` // 200 - 500
}

// dataval, _ := json.Marshal(req) untuk example value
// type DtoHelperRedisRequest struct {
// 	Key   string        `json:"key" bson:"key"`
// 	Value string        `json:"value" bson:"value"`
// 	Exp   time.Duration `json:"exp" bson:"exp"`
// }

// type DtoHelperRedisReturn struct {
// 	Error error  `json:"error" bson:"error"`
// 	Value string `json:"value" bson:"value"`
// }

type DtoPageSuccess struct {
	TotalRows  int64   `json:"total_rows"  bson:"total_rows"`   // total data di database
	Limit      int64   `json:"limit"  bson:"limit"`             // limit
	TotalPages float64 `json:"total_pages"  bson:"total_pages"` // total_rows di bagi limit
	Current    int64   `json:"current"  bson:"current"`         // user sedang buka page berapa
}

type StoreResponseHttp struct {
	ServiceName     string         `json:"service_name" bson:"service_name"`
	UserId          uint64         `json:"user_id" bson:"user_id"`
	Code            int            `json:"code" bson:"code"`
	Request         string         `json:"request" bson:"request"`
	Response        string         `json:"response" bson:"response"`
	Data            any            `json:"data" bson:"data"`
	Message         string         `json:"message" bson:"message"`
	MessageActivity string         `json:"message_activity" bson:"message_activity"`
	Page            DtoPageSuccess `json:"page" bson:"page"`
	LogCode         string         `json:"log_code" bson:"log_code"`
	ErrorResponse   string         `json:"error_response" bson:"error_response"`
	Logging         bool           `json:"logging" bson:"logging"`
}

type DtoResponseService struct {
	Code    int            `json:"code" bson:"code"`
	Message string         `json:"message" bson:"message"`
	Page    DtoPageSuccess `json:"page" bson:"page"`
	Error   string         `json:"error" bson:"error"`
}

type TimezoneLocation struct {
	Status      string  `json:"status"`
	Country     string  `json:"country"`
	CountryCode string  `json:"countryCode"`
	Region      string  `json:"region"`
	RegionName  string  `json:"regionName"`
	City        string  `json:"city"`
	Zip         string  `json:"zip"`
	Latitude    float64 `json:"lat"`
	Longitude   float64 `json:"lon"`
	TimezoneLoc string  `json:"timezone"`
	Isp         string  `json:"isp"`
	Org         string  `json:"org"`
	As          string  `json:"as"`
	Query       string  `json:"query"`
}

type LoginResponse struct {
	AccessToken     string `json:"access_token"`
	XIdempotencyKey string `json:"x_idempotency_key"`
}

type JwtEntity struct {
	Id uint64 `json:"Id"`
	jwt.RegisteredClaims
}

type TokenEntity struct {
	Id                   uint64 `json:"id"`
	RefreshToken         string `json:"refresh_token"`
	EncrypteToken        string `json:"encrypte_token"`
	XIdempotencyKeyToken string `json:"x_idempotency_key_token"`
	XIdempotencyKey      string `json:"x_idempotency_key"`
}

type ReqAcccess struct {
	Key    int    `json:"key"`
	Action string `json:"action"` // view,create,update,delete,approval
}

type ShowAccount struct {
	Id uint64 `json:"id"`
}
