package common

import (
	"time"

	"gitlab.com/varriete-corp-common/jastip/utils/config"

	"github.com/spf13/viper"
)

// Asia/Jakarta
// Asia/Pontianak
// Asia/Makassar
// Asia/Jayapura

type Time struct{}

// ----------------------------------------------------------------------------------------------- baru ini

func (Group Time) TimeNow() time.Time {
	Cv := config.Config{}.ConfigVariable()
	TimelocUse := Cv.TimeLocation
	Loc, _ := time.LoadLocation(TimelocUse)
	Now := time.Now().In(Loc)
	return Now
}

func (Group Time) TimeNowUnix(TypeNano string) int64 { // https://gosamples.dev/unix-time/
	var Response int64

	switch TypeNano {
	case "Unix":
		Response = Group.TimeNow().Unix()
	case "UnixMilli":
		Response = Group.TimeNow().UnixMilli()
	case "UnixMicro":
		Response = Group.TimeNow().UnixMicro()
	case "UnixNano":
		Response = Group.TimeNow().UnixNano()
	default:
		Response = Group.TimeNow().Unix()
	}

	return Response
}

func (Group Time) TimeRfc3339() string {
	TimeNow := Group.TimeNow()
	TimeUse := TimeNow.Format(time.RFC3339)
	return TimeUse
}

func (Group Time) TimeToRfc3339(TimeReq string) string {
	Cv := config.Config{}.ConfigVariable()
	TimelocUse := Cv.TimeLocation

	TimeFormat, _ := time.Parse(time.RFC3339, TimeReq)
	Loc, _ := time.LoadLocation(TimelocUse)
	TimeFormat = TimeFormat.In(Loc)

	Resp := TimeFormat.String()
	return Resp
}

// -----------------------------------------------------------------------------------------------

func (Group Time) TimeNowYmdHis() string {
	Cv := config.Config{}.ConfigVariable()
	TimelocUse := Cv.TimeLocation

	Loc, _ := time.LoadLocation(TimelocUse)
	Now := time.Now().In(Loc)
	Ymdhis := Now.Format(viper.GetString("time_format.date_time_ymdhis"))
	return Ymdhis
}

func (Group Time) TimeNowYmd() string {
	Cv := config.Config{}.ConfigVariable()
	TimelocUse := Cv.TimeLocation

	Loc, _ := time.LoadLocation(TimelocUse)
	Now := time.Now().In(Loc)
	Ymdhis := Now.Format(viper.GetString("time_format.date_ymd"))
	return Ymdhis
}

func (Group Time) AddDate(Year int, Month int, Days int) time.Time {
	Now := Group.TimeNow()
	AddTime := Now.AddDate(Year, Month, Days)
	return AddTime
}

func (Group Time) AddHours(Hour int) time.Time {
	Now := Group.TimeNow()
	AddHour := Now.Add(time.Hour * time.Duration(Hour))
	return AddHour
}

func (Group Time) AddMinutes(Minutes int) time.Time {
	Now := Group.TimeNow()
	AddHour := Now.Add(time.Minute * time.Duration(Minutes))
	return AddHour
}

func (Group Time) AddSecond(Second int) time.Time {
	Now := Group.TimeNow()
	AddHour := Now.Add(time.Second * time.Duration(Second))
	return AddHour
}

func (Group Time) LastDateOfMonth(Year int, Month time.Month) time.Time {
	Cv := config.Config{}.ConfigVariable()
	TimelocUse := Cv.TimeLocation

	//-------------------- example
	// loc, _ := time.LoadLocation("Asia/Jakarta")

	// timebulan := time.Month(8)

	// datatime := time.Date(2023, timebulan+1, 0, 0, 0, 0, 0, loc)
	// fmt.Println("last date : ", datatime)
	//--------------------

	Loc, _ := time.LoadLocation(TimelocUse)
	return time.Date(Year, Month+1, 0, 0, 0, 0, 0, Loc)
}
